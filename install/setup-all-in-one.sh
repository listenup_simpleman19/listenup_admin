#!/bin/bash -e

# This script performs a complete install on a single host. Intended for
# development, staging and testing.

GITROOT=git@bitbucket.org:simpleman19
pushd ~/

# Get RSA keys for bitbucket.org
ssh-keyscan bitbucket.org >> ~/.ssh/known_hosts

# environment variables
export HOST_IP_ADDRESS=$(ip route get 1 | awk '{print $NF;exit}')
export SECRET_KEY=$(pwgen -1 -c -n -s 32)
export JWT_SECRET_KEY=$(pwgen -1 -c -n -s 32)
export PATH=$PATH:$PWD/listenup_admin/bin
echo export HOST_IP_ADDRESS=$HOST_IP_ADDRESS >> ~/.profile
echo export SECRET_KEY=$SECRET_KEY >> ~/.profile
echo export JWT_SECRET_KEY=$JWT_SECRET_KEY >> ~/.profile
echo export PATH=\$PATH:$PWD/listenup_admin/bin >> ~/.profile

# logging
docker run --name logspout -d --restart always -p 1095:80 -v /var/run/docker.sock:/var/run/docker.sock gliderlabs/logspout:latest

# download the code and build containers
git clone $GITROOT/listenup_docker
cd listenup_docker/easy-lb-haproxy
./build.sh
cd ../easy-etcd
./build.sh
cd ../redis-with-pass
./build.sh
cd ~/

# deploy etcd
docker run --name etcd -d --restart always -p 2379:2379 -p 2380:2380 simpleman19/easy-etcd:latest
export ETCD=http://$HOST_IP_ADDRESS:2379
echo export ETCD=$ETCD >> ~/.profile

# install etcdtool
docker pull mkoppanen/etcdtool

# deploy postgres
mkdir -p ~/postgres-data
POSTGRES_ROOT_PASSWORD=$(pwgen -1 -c -n -s 16)
docker run --name postgres --restart always -e POSTGRES_PASSWORD=$POSTGRES_ROOT_PASSWORD -e POSTGRES_USER=root -e POSTGRES_DB=listenup -p 5432:5432 -v ~/postgres-data:/var/lib/postgresql/data -d postgres:10
export DATABASE_SERVER=$HOST_IP_ADDRESS:5432
echo export DATABASE_SERVER=$DATABASE_SERVER >> ~/.profile
echo POSTGRES_ROOT_PASSWORD=$POSTGRES_ROOT_PASSWORD >> ~/.postgres_root_password

# download the code and build containers
git clone $GITROOT/listenup_admin
cd listenup_admin
source mfvars
install/setup_redis.sh

# deploy load balancer
# use the haproxy load balancer (recommended)
echo export LOAD_BALANCER=haproxy >> ~/.profile
docker run --name lb -d --restart always -p 80:80 -e ETCD_PEERS=$ETCD -e HAPROXY_STATS=1 simpleman19/easy-lb-haproxy:latest


# Create db passwords
source mfvars
install/make-db-passwords.sh
echo "source $PWD/mfvars" >> ~/.profile
mfclone ..
mfbuild all

# run services
for SERVICE in $SERVICES; do
    mfrun $SERVICE
done

popd
echo Listenup is now deployed!
echo - Run "source ~/.profile" or log back in to update your environment.
echo - You may need some variables from ~/.profile if you intend to run services in another host.
