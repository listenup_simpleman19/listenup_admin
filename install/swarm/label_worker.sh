#!/bin/bash

if [[ "$1" == "--help" ]]; then
  echo "./label_worker <user> <manager_ip> <hostname-to-label>"
  exit 0
fi

USER=$1
MANAGER_IP=$2
HOSTNAME=$3
TYPE=
if [[ -z "$4" ]]; then
  PS3='Please enter host type: '
  options=("files" "loadbalancer" "manager" "worker" "db" "backup" "all" "Quit")
  select opt in "${options[@]}"
  do
      case $opt in
          "files")
              echo "Setting up a server to serve files"
              TYPE=$opt
              break
              ;;
          "loadbalancer")
              echo "Setting up a server to serve files"
              TYPE=$opt
              break
              ;;
          "manager")
              echo "Setting up a server to be a manager"
              TYPE=$opt
              break
              ;;
          "worker")
              echo "Setting up a server to be a generic worker"
              TYPE=$opt
              break
              ;;
          "db")
              echo "Setting up a server to be a db server"
              TYPE=$opt
              break
              ;;
          "backup")
              echo "Setting up a server to be a backup server"
              TYPE=$opt
              break
              ;;
          "all")
              echo "Setting up a server to be everything"
              TYPE=$opt
              break
              ;;
          "Quit")
              break
              ;;
          *) echo "invalid option $REPLY";;
      esac
  done
else
  TYPE=$4
fi

LOCATION=
if [[ -z "$5" ]]; then
  PS3='Please enter host location: '
  options=("home" "Quit")
  select opt in "${options[@]}"
  do
      case $opt in
          "home")
              LOCATION=$opt
              break
              ;;
          "Quit")
              break
              ;;
          *) echo "invalid option $REPLY";;
      esac
  done
else
  LOCATION=$5
fi

RACK=
if [[ -z "$6" ]]; then
echo "Which rack is this in?: "
read RACK
else
  RACK=$6
fi

echo "Server in: $LOCATION and in rack: $RACK"

DEVELOPMENT=false
if [[ -z "$7" ]]; then
PS3='Is this a dev machine?: '
options=("yes" "no" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "yes")
            DEVELOPMENT=true
            break
            ;;
        "no")
            DEVELOPMENT=false
            break
            ;;
        "Quit")
            break
            ;;
        *) echo "invalid option $REPLY";;
    esac
done
else
DEVELOPMENT=$7
fi

if [[ "$DEVELOPMENT" == true ]]; then
    echo "Setting up a development machine"
fi


FILE_TYPES=
if [[ ${TYPE} == 'files' ]] || [[ ${TYPE} == 'all' ]]; then
  if [[ -z "$8" ]]; then
    PS3='Please enter types of files allowed: '
    options=("audio" "image" "attachment" "all" "Quit")
    select opt in "${options[@]}"
    do
        case $opt in
            "audio" | "image" | "attachment" | "all")
                FILE_TYPES=$opt
                break
                ;;
            "Quit")
                break
                ;;
            *) echo "invalid option $REPLY";;
        esac
    done
  else
    FILE_TYPES=$8
  fi
  echo "Setting up a file server to service types of: $FILE_TYPES"
fi


NODE_LABELS="--label-add location=$LOCATION --label-add rack=$RACK"

if [[ ${TYPE} == 'files' ]] || [[ ${TYPE} == 'all' ]]; then
  NODE_LABELS="$NODE_LABELS --label-add files=true --label-add filetype=$FILE_TYPES"
fi
if [[ ${TYPE} == 'loadbalancer' ]] || [[ ${TYPE} == 'all' ]]; then
  NODE_LABELS="$NODE_LABELS --label-add loadbalancer=true"
fi
if [[ ${TYPE} == 'db' ]] || [[ ${TYPE} == 'all' ]]; then
  NODE_LABELS="$NODE_LABELS --label-add database=true"
fi
if [[ ${TYPE} == 'backup' ]] || [[ ${TYPE} == 'all' ]]; then
  NODE_LABELS="$NODE_LABELS --label-add backup=true"
fi
if [[ ${TYPE} == 'worker' ]] || [[ ${TYPE} == 'all' ]]; then
  NODE_LABELS="$NODE_LABELS --label-add generic=true"
fi
if [[ "$DEVELOPMENT" == true ]]; then
  NODE_LABELS="$NODE_LABELS --label-add development=true"
fi

echo "Running on manager: docker node update $NODE_LABELS $HOSTNAME"
ssh -t ${USER}@${MANAGER_IP} -C "docker node update $NODE_LABELS $HOSTNAME"
