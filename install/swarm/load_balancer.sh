#!/bin/bash -e

# This script performs a complete install on a single host. Intended for
# development, staging and testing.

GITROOT=git@bitbucket.org:simpleman19
pushd ~/

# Get RSA keys for bitbucket.org
ssh-keyscan bitbucket.org >> ~/.ssh/known_hosts

# environment variables
export HOST_IP_ADDRESS=$(ip route get 1 | awk '{print $7;exit}')

# logging
docker run --name logspout -d --restart always -p 1095:80 -v /var/run/docker.sock:/var/run/docker.sock gliderlabs/logspout:latest

# download the code and build containers
git clone $GITROOT/listenup_docker
cd listenup_docker/easy-lb-haproxy
./build.sh
cd ../easy-etcd
./build.sh
cd ~/

# deploy etcd
docker run --name etcd -d --restart always -p 2379:2379 -p 2380:2380 simpleman19/easy-etcd:latest
export ETCD=http://$HOST_IP_ADDRESS:2379
echo export ETCD=$ETCD >> ~/.profile

# install etcdtool
docker pull mkoppanen/etcdtool

# deploy load balancer
# use the haproxy load balancer (recommended)
echo export LOAD_BALANCER=haproxy >> ~/.profile
docker run --name lb -d --restart always -p 80:80 -e ETCD_PEERS=$ETCD -e HAPROXY_STATS=1 simpleman19/easy-lb-haproxy:latest

docker run -d -p 5000:5000 --restart=always --name registry-srv registry:2
#docker run -d -p 8080:8080 --name registry-web --link registry-srv -e REGISTRY_URL=http://registry-srv:5000/v2 -e REGISTRY_NAME=localhost:5000 hyper/docker-registry-web

# deploy postgres
mkdir -p ~/postgres-data
POSTGRES_ROOT_PASSWORD=$(pwgen -1 -c -n -s 16)
docker run --name postgres --restart always -e POSTGRES_PASSWORD=$POSTGRES_ROOT_PASSWORD -e POSTGRES_USER=root -e POSTGRES_DB=listenup -p 5432:5432 -v ~/postgres-data:/var/lib/postgresql/data -d postgres:10
export DATABASE_SERVER=$HOST_IP_ADDRESS:5432
echo export DATABASE_SERVER=$DATABASE_SERVER >> ~/.profile
echo POSTGRES_ROOT_PASSWORD=$POSTGRES_ROOT_PASSWORD >> ~/.postgres_root_password

PW_FILE=~/.redis_password
if [[ ! -f $PW_FILE ]]; then
    touch $PW_FILE
fi

source $PW_FILE

VAR=$(echo REDIS_DB_PASSWORD | tr '/a-z/' '/A-Z/')
if [[ "$(eval echo \$$VAR)" == "" ]]; then
    PASSWORD=$(pwgen -1 -c -n -s 16)
    echo "${VAR}=$PASSWORD" >> $PW_FILE
fi

source ~/.redis_password
VAR=$(echo REDIS_DB_PASSWORD | tr '/a-z/' '/A-Z/')
REDIS_PASSWORD=$(eval echo \$$VAR)
echo export REDIS_DB_PASSWORD=$REDIS_PASSWORD >> ~/.profile

# deploy redis
docker run --name redis -d --restart always -e REDIS_PASSWORD=$REDIS_PASSWORD -p 6379:6379 simpleman19/redis-with-pass:latest
export REDIS=$HOST_IP_ADDRESS:6379
echo export REDIS=$REDIS >> ~/.profile


popd
echo Loadbalancer is now deployed!
echo - Run "source ~/.profile" or log back in to update your environment.
echo - You may need some variables from ~/.profile if you intend to run services in another host.
