#!/bin/bash

# This is a script that will setup a new ubuntu 18.04 vm with everything

if [[ "$1" == "--help" ]]; then
  echo "Usage: setup-vm.sh <vm_ip> [type {manager worker}] [user] [manager_ip] [hostname]"
  exit 0
fi

VM_IP=$1

TYPE=
if [[ -z "$2" ]]; then
  echo "Type: "
  read TYPE
else
  TYPE=$2
fi

USERNAME=
if [[ -z "$3" ]]; then
  echo "User: "
  read USERNAME
else
  USERNAME=$3
fi

MANAGER_IP=
if [[ -z "$4" ]]; then
  echo "Manager ip: "
  read MANAGER_IP
else
  MANAGER_IP=$4
fi

NEW_HOSTNAME=
if [[ -z "$5" ]]; then
  echo "Hostname: "
  read NEW_HOSTNAME
else
  NEW_HOSTNAME=$5
fi

ssh-copy-id $USERNAME@$VM_IP

scp setup-worker.sh $USERNAME@$VM_IP:/tmp/setup-worker.sh
scp daemon.json $USERNAME@$VM_IP:/tmp/daemon.json

JOIN_TOKEN="SWMTKN-1-4k6weozcc02htf9k08tgtg24lvqe59w02jaygze7k8ygibam5i-29pg6iqwq5ofc773r8gy4f353"

ssh -t $USERNAME@$VM_IP -C "cd /tmp && sudo su -c \"./setup-worker.sh $USERNAME $NEW_HOSTNAME $TYPE $MANAGER_IP $JOIN_TOKEN\""

echo "Waiting for node to reboot"
sleep 15

./label_worker.sh $USERNAME $MANAGER_IP $NEW_HOSTNAME
