#!/bin/bash

# This script installs all the requireed dependencies on a host, which is
# assumed to be Ubuntu 18.04.

if [ "$1" == "--help" ]; then
  echo "Usage: setup-worker.sh <USER> <HOSTNAME> <TYPE> <MANAGER_IP> [JOIN_TOKEN]"
  echo "USER: user to ssh into worker with"
  echo "HOSTNAME: hostname to change the worker's hostname to"
  echo "TYPE: type of server ex: files, all, database..."
  echo "MANAGER_IP: ip address of the primary swarm manager"
  echo "JOIN_TOKEN: token to use to join the swarm"
  exit 0
fi

USERNAME=$1
NEW_HOSTNAME=$2
TYPE=$3
ADVERTISE_IP=$4
JOIN_TOKEN="$5"
CURRENT_HOSTNAME=$(cat /etc/hostname)

# install system dependencies
apt-get update -y
apt-get install -y curl software-properties-common postgresql-client

sed -i 's/'$CURRENT_HOSTNAME'/'$NEW_HOSTNAME'/g' /etc/hosts
sed -i 's/'$CURRENT_HOSTNAME'/'$NEW_HOSTNAME'/g' /etc/hostname

sed -i 's/preserve_hostname: false/preserve_hostname: true/g' /etc/cloud/cloud.cfg

hostnamectl set-hostname $NEW_HOSTNAME

# install docker
curl -fsSL https://get.docker.com/ | sh
usermod -aG docker $USERNAME

cp /tmp/daemon.json /etc/docker/daemon.json

service docker restart

if [ $TYPE == 'worker' ]; then
  # Docker swarm join, should probably be a better way than editing this but...
  docker swarm join --token ${JOIN_TOKEN} ${ADVERTISE_IP}:2377
elif [ $TYPE == 'manager' ]; then
  docker swarm init --advertise-addr ${ADVERTISE_IP}
  docker network create --attachable --driver overlay --subnet=10.11.0.0/16 listenup_net
  docker secret create registry_htpasswd /tmp/htpasswd
fi

reboot
