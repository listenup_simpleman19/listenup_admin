#!/bin/bash

source .common

MAX_THREADS=4

set_max_threads $MAX_THREADS

service_create () {
  NAME=$1
  ARGS=$2

  ssh -q ${DOCKER_REMOTE_USER}@${MANAGER_IP} -C "docker service ls --format '{{.Name}}' | grep ${NAME}"
  retVal=$?
  if [ $retVal -eq 0 ]; then
    echo "Service ${NAME} already exists"
  elif [ $retVal -eq 255 ]; then
    echo "Could not connect over ssh to: ${DOCKER_REMOTE_USER}@${MANAGER_IP}"
    exit 255
  else
    ssh -q ${DOCKER_REMOTE_USER}@${MANAGER_IP} -C -t "docker service create --name $NAME $ARGS"
    echo "Service ${NAME} created"
  fi

}

service_create_private () {
  NAME=$1
  ARGS=$2

  ssh -q ${DOCKER_REMOTE_USER}@${MANAGER_IP} -C "docker service ls --format '{{.Name}}' | grep ${NAME}"
  retVal=$?
  if [ $retVal -eq 0 ]; then
    echo "Service ${NAME} already exists"
  elif [ $retVal -eq 255 ]; then
    echo "Could not connect over ssh to: ${DOCKER_REMOTE_USER}@${MANAGER_IP}"
    exit 255
  else
    ssh -q ${DOCKER_REMOTE_USER}@${MANAGER_IP} -C -t "docker login ${REGISTRY} && docker service create --with-registry-auth --name $NAME $ARGS"
    echo "Service ${NAME} created"
  fi
}

build() {
    echo "---------------- Building: $1 ----------------"
    pushd $LD/$1 || exit
    command_with_retry lubuild
    command_with_retry luregistrypush
    popd || exit
}

run() {
    echo "---------------- Running: $1 ----------------"
    pushd $LD/$1 || exit
    command_with_retry lurun
    popd || exit
}

threaded_run service_create portainer "--publish 9000:9000 --constraint 'node.role == manager' --mount type=bind,src=/var/run/docker.sock,dst=/var/run/docker.sock portainer/portainer -H unix:///var/run/docker.sock --admin-password='\$2y\$05\$4OTqkKbRBO6Yzb2FJXqXIe04IbYbzQausKAupgQT2z0tI4KsN2YAK'"

# new nocix call docker service create --name registry-srv --replicas 1 --constraint 'node.role == manager' --network listenup_net --publish 5000:5000 -e "REGISTRY_AUTH=htpasswd" -e "REGISTRY_AUTH_HTPASSWD_REALM=Registry Realm" -e "REGISTRY_AUTH_HTPASSWD_PATH=/auth/htpasswd" --secret src=registry_htpasswd,target="/auth/htpasswd" registry:2
threaded_run service_create registry-srv "--replicas 1 --constraint 'node.role == manager' --network listenup_net --publish 5000:5000 -e \"REGISTRY_AUTH=htpasswd\" -e \"REGISTRY_AUTH_HTPASSWD_REALM=Registry Realm\" -e \"REGISTRY_AUTH_HTPASSWD_PATH=/auth/htpasswd\" --secret src=registry_htpasswd,target=\"/auth/htpasswd\" registry:2"

wait_all_threads

echo "-------------------------------"
echo "-------------------------------"

if [[ "$DOCKER_USERNAME" == "" ]] || [[ "$DOCKER_PASSWORD" == "" ]]; then
    ssh -q ${DOCKER_REMOTE_USER}@${MANAGER_IP} -C -t "docker login ${REGISTRY}"
else
    ssh -q ${DOCKER_REMOTE_USER}@${MANAGER_IP} -C -t "docker login ${REGISTRY} -u $DOCKER_USERNAME -p $DOCKER_PASSWORD"
fi


docker login ${REGISTRY}

pushd $LD/listenup_docker || exit
command_with_retry ./build.sh

command_with_retry ./push.sh
popd || exit

threaded_run service_create_private etcd "--publish 2379:2379 --constraint 'node.role == manager' --publish 2380:2380 --network listenup_net ${REGISTRY}/simpleman19/easy-etcd:latest"

threaded_run service_create_private redis "-e REDIS_PASSWORD=$REDIS_PASSWORD -p 6379:6379 --network listenup_net ${REGISTRY}/simpleman19/redis-with-pass:latest"

threaded_run service_create_private influxdb "-e INFLUX_USER=${INFLUX_USER} -e INFLUX_PASSWORD=${INFLUX_PASSWORD} -p 8086:8086 --network listenup_net ${REGISTRY}/listenup/influxdb-with-pass:latest"

threaded_run service_create_private postgres "--constraint 'node.labels.database == true' --network listenup_net -e POSTGRES_PASSWORD=$POSTGRES_PASSWORD -e POSTGRES_USER=$POSTGRES_USER -e POSTGRES_DB=listenup -p 5432:5432 --mount type=volume,source=postgres-volume,destination=/var/lib/postgresql/data postgres:11.5 postgres -N 500"

wait_all_threads

service_create_private lb "--publish 80:80 --network listenup_net --constraint 'node.labels.loadbalancer == true' -e ETCD_PEERS=http://etcd:2379 -e HAPROXY_STATS=1 ${REGISTRY}/simpleman19/easy-lb-haproxy:latest" || true

build "listenup_common"

for service in ${SERVICES}
do
  threaded_run build "listenup_${service}"
done

wait_all_threads

for service in ${SERVICES}
do
  threaded_run run "listenup_${service}"
done

wait_all_threads

# Node with ip
# for NODE in $(docker node ls --format '{{.Hostname}}'); do echo -e "${NODE} - $(docker node inspect --format '{{.Status.Addr}}' "${NODE}")"; done
