#!/bin/bash

./make-redis-password.sh

source ~/.redis_password
VAR=$(echo REDIS_DB_PASSWORD | tr '/a-z/' '/A-Z/')
REDIS_PASSWORD=$(eval echo \$$VAR)
echo export REDIS_DB_PASSWORD=$REDIS_PASSWORD >> ~/.profile

# deploy redis
docker run --name redis -d --restart always -e REDIS_PASSWORD=$REDIS_PASSWORD -p 6379:6379 simpleman19/redis-with-pass:latest
export REDIS=$HOST_IP_ADDRESS:6379
echo export REDIS=$REDIS >> ~/.profile
