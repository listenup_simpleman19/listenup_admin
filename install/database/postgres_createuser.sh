SERVER=$1
DATABASE=$2
USER=$3
PASSWORD=$4

echo "Creating a database user"

source ../../configure/.passwords
source ../../configure/.config

PGPASSWORD="$POSTGRES_PASSWORD" psql -U $POSTGRES_USER -h $SERVER -lqt | cut -d \| -f 1 | grep -qw $DATABASE
retVal=$?
if  [ $retVal -ne 0  ]; then
  echo "Database does not exist: $DATABASE"
fi

PGPASSWORD="$POSTGRES_$POSTGRES_USER_PASSWORD" psql -U $POSTGRES_USER -h $SERVER postgres -tAc "SELECT 1 FROM pg_roles WHERE rolname='$DATABASE'" | grep -q 1
retVal=$?
if  [ $retVal -ne 0  ]; then
  echo "Setting up user for database: $DATABASE"
  PGPASSWORD="$POSTGRES_$POSTGRES_USER_PASSWORD" psql -U $POSTGRES_USER -h $SERVER postgres -c "CREATE USER $DATABASE WITH ENCRYPTED PASSWORD '$PASSWORD';"
  PGPASSWORD="$POSTGRES_$POSTGRES_USER_PASSWORD" psql -U $POSTGRES_USER -h $SERVER postgres -c "GRANT ALL PRIVILEGES ON DATABASE $DATABASE TO $DATABASE;"
else
  echo "User already exists"
fi
