SERVER=$1
DATABASE=$2

echo "Creating a database"

source ../../configure/.passwords
source ../../configure/.config

PGPASSWORD="$POSTGRES_PASSWORD" psql -U $POSTGRES_USER -h $SERVER -lqt | cut -d \| -f 1 | grep -qw $DATABASE
retVal=$?
if  [ $retVal -ne 0  ]; then
  echo "Creating Database: $DATABASE"
  PGPASSWORD="$POSTGRES_$POSTGRES_USER_PASSWORD" createdb -U $POSTGRES_USER -h $SERVER $DATABASE
else
  echo "Database already exists"
fi
