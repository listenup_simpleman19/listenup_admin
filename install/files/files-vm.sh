#!/bin/bash

# This is a script that will setup a new ubuntu 18.04 vm with everything

VM_IP=$1
USER=$2
HOSTNAME=$3

ssh-copy-id $USER@$VM_IP

scp setup-files.sh $USER@$VM_IP:/tmp/setup-files.sh
ssh $USER@$VM_IP -C "mkdir -p /tmp/.ssh"
scp ../../ro_keys/id_rsa $USER@$VM_IP:/tmp/.ssh/id_rsa
scp ../../ro_keys/id_rsa.pub $USER@$VM_IP:/tmp/.ssh/id_rsa.pub

ssh -t $USER@$VM_IP -C "cd /tmp && sudo su -c \"./setup-files.sh $USER $HOSTNAME\""
