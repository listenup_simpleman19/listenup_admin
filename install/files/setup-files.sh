#!/bin/bash

# This script installs all the requireed dependencies on a host, which is
# assumed to be Ubuntu 18.04.

# install system dependencies
apt-get update -y
apt-get install -y curl software-properties-common

cp /tmp/.ssh/* /home/$1/.ssh/
rm -rf /tmp/.ssh
chown $1:$1 /home/$1/.ssh/id_rsa*
chmod 600 /home/$1/.ssh/id_rsa*

chown -R nobody:nogroup /export

CURRENT_HOSTNAME=$(cat /etc/hostname)
sed -i 's/'$CURRENT_HOSTNAME'/'$2'/g' /etc/hosts
sed -i 's/'$CURRENT_HOSTNAME'/'$2'/g' /etc/hostname

sed -i 's/preserve_hostname: false/preserve_hostname: true/g' /etc/cloud/cloud.cfg

hostnamectl set-hostname $2
# install docker
curl -fsSL https://get.docker.com/ | sh
usermod -aG docker $1

service docker restart

reboot
