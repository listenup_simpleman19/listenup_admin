#!/bin/bash

vagrant snapshot list | grep initial_snapshot
if [[ $? -eq 0 && $1 != "--force" ]]; then
    vagrant snapshot restore default initial_snapshot
    sleep 20
else
    vagrant up
    sleep 20
    vagrant snapshot save default initial_snapshot
fi

source luconfig vagrant --docker

pushd install/swarm || exit
./label_worker.sh vagrant 192.168.33.10 vagrantManager all home 1 false all
sleep 1

ssh 192.168.33.10 "sudo mkdir /uploads && sudo chown -R nobody:nogroup /uploads"
export DOCKER_USERNAME="node"
export DOCKER_PASSWORD="password"
./services.sh

docker swarm leave --force || true
# shellcheck disable=SC2091
$(ssh 192.168.33.10 "docker swarm join-token manager | grep -i token | sed -e 's/^[ \t]*//'")
./label_worker.sh vagrant 192.168.33.10 $HOSTNAME all home 1 true all

popd || exit

