import json
import os
import uuid
import sys
import re

configs = {
    'vagrant': '-vagrant',
    'public': '-public',
}


def variable_replacement(strings, config_file):
    output = []
    replace = []
    if not isinstance(strings, (list, tuple)):
        replace = [strings]
    else:
        replace = strings
    for string in replace:
        found = re.finditer(r"\${.+}", string)
        for f_match in found:
            key = f_match.group()
            key = key.replace('$', '')
            key = key.replace('{', '')
            key = key.replace('}', '')
            string = string.replace(f_match.group(), variable_replacement(config_file[key], config_file))
        output.append(string)
    if not isinstance(strings, (list, tuple)) and len(output) == 1:
        return output[0]
    elif not isinstance(strings, (list, tuple)) and len(output) == 0:
        return None
    elif not isinstance(strings, (list, tuple)) and len(output) > 1:
        raise Exception
    return output


def write_bash_var_file(path, dictionary):
    with open(path, 'w') as f:
        for k, v in dictionary.items():
            if isinstance(v, (list, tuple)):
                values = ""
                for val in v:
                    values += str(val).strip() + " "
                f.write("export " + str(k).strip() + "=\"" + values.strip() + "\"\n")
            else:
                f.write("export " + str(k).strip() + "=" + str(v).strip() + '\n')


if __name__ == '__main__':
    if len(sys.argv) <= 1 or len(sys.argv) > 3:
        print("Invalid arguments, please provide which config you would like to load")
        raise Exception("Invalid args")
    if sys.argv[1] not in configs:
        print(str(sys.argv[1]) + " not found in valid configs")
    config_ext = configs[sys.argv[1]]

    # Config File
    with open('config' + config_ext + '.json') as f:
        data = json.load(f)
    for k, v in data.items():
        data[k] = variable_replacement(v, data)
    write_bash_var_file('.config', data)
    with open('config.json', 'w') as f:
        json.dump({k: v for k, v in data.items() if not isinstance(v, list)}, f, indent=4)
        # Pulling out arrays due to lack of pycharm support

    # Password generation
    loaded = None
    if os.path.isfile('passwords.json'):
        with open('passwords.json', 'r') as f:
            loaded = json.load(f)
    with open('password' + config_ext + '.json', 'r') as f:
        pass_temp = json.load(f)
    if loaded:
        for k in pass_temp:
            if k in loaded:
                pass_temp[k] = loaded[k]
    for k, v in pass_temp.items():
        if v == 'generate':
            pass_temp[k] = str(uuid.uuid4()).replace('-', '')[:16]
    with open('passwords.json', 'w') as f:
        json.dump({k: v for k, v in pass_temp.items() if not isinstance(v, list)}, f, indent=4)
        # Pulling out arrays due to lack of pycharm support

    write_bash_var_file('.passwords', pass_temp)

    print("Configured")
