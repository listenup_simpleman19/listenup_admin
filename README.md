Listenup Administration Repo

This project is developed on manjaro and is deployed on ubuntu server 18.04

Prereqs: virtualbox, vagrant with vagrant-disksize, node, python3, and virtualenv installed globally using pip

Steps to startup:
1. mkdir -p listenup_dev/wheels
2. cd listenup_dev
3. clone this repo
4. add the path to listenup_dev/listenup_admin/bin to your path (I do this in my bashrc)
5. source luconfig vagrant (This will setup your env for running with vagrant)
6. cd ..
7. luclone . (This should clone all repos and create all the virtual envs)
8. lubuildall (This will go through each repo and build it)
9. cd listenup_admin
10. sudo echo "http://192.168.33.1:81" >> /url
11. ./vagrant_up.sh
	Registry username: node
	Registry password: password
