import sys
sys.path.append("remote/pycharm-debug.egg")
sys.path.append("remote/pycharm-debug-py3k.egg")

import pydevd
pydevd.settrace('192.168.33.1', port=12345, stdoutToServer=True, stderrToServer=True)

from wsgi import app
