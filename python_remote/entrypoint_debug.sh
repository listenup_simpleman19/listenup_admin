#!/bin/bash

echo "Starting in debugging mode"

if [[ -z "$PYTHONPATH" ]]; then
    export PYTHONPATH="remote/pycharm-debug.egg:remote/pycharm-debug-py3k.egg"
else
    export PYTHONPATH="$PYTHONPATH:remote/pycharm-debug.egg:remote/pycharm-debug-py3k.egg"
fi

./entrypoint.sh --debug